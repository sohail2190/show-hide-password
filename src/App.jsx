import { useState } from 'react'
import './App.css'

function App() {
  
const [pswd, setPswd] = useState('');
const [isPswd, setIsPswd] = useState(false);



  return (
    <>
     <div className="container">
      <h3>Show and Hide password with React</h3>
      <div className="pswd-container">
          <input 
          name='pswd'
          placeholder='enter your password'
          type={isPswd ? 'text' : 'password'}
          value={pswd}
          onChange={(e) => setPswd(e.target.value)} />
      </div>

      <button onClick={()=> setIsPswd(prevState => !prevState)}>
      {isPswd ? 'Hide Password' : 'Show Password'}

      </button>
     </div>
    </>
  )
}

export default App
